<?php

namespace Softko\User;

use Softko\Captcha\CaptchaInterface;
use Softko\Db\UserDbInterface;

class User
{
	const MAX_LOGIN_ATTEMPTS = 5;
	const FAILED_LOGIN_DELAY_MULTIPLIER = 2;
	const FAIL_SLEEP = 2; // seconds

	const REMEMBER_LENGTH = 20;
	const REMEMBER_EXPIRE = 30; // days
	const REMEMBER_COOKIE_NAME = 'remember';
	const REMEMBER_UA_MIN_SIMILARITY = 90; // percents, 0 for not checking User agent

	const PASSWORD_MIN_LENGTH = 8;
	const PASSWORD_ALGORITHM = PASSWORD_BCRYPT;
	const PASSWORD_OPTIONS = [];
	const RECOVER_LENGTH = 50;
	const RECOVER_EXPIRE = 7200; // seconds

	const CIPHER = 'AES-256-CBC';

	protected UserDbInterface $db;
	protected CaptchaInterface $captcha;
	protected string $ip;
	protected string $userAgent;
	private int $failedLoginCount = 0;
	protected string $error = '';
	private string $cipherKey = 'KeyKeyKeyKeyKeyKeyKeyKeyKeyKeyKe';
	private string $cipherIv = 'ivIVivIVivIVivIV';

	public function __construct($db, CaptchaInterface $captcha, string $ip, string $userAgent)
	{
		$this->db = $db;
		$this->captcha = $captcha;
		$this->ip = $ip;
		$this->userAgent = $userAgent;
	}

	public function isLogged(): bool
	{
		return isset($_SESSION['user']);
	}

	/**
	 * Verifies user login attempt
	 *
	 * @param string $email
	 * @param string $password
	 * @param bool   $rememberMe
	 *
	 * @return bool
	 */
	public function login(string $email, string $password, bool $rememberMe): bool
	{
		$failedByIp = $this->db->getFailedLoginCountByIp($this->encryptString($this->ip));
		if ($failedByIp > static::MAX_LOGIN_ATTEMPTS) {
			$this->error = 'Too many failed login tries from your ip address, try again later';
			$this->sleep(static::FAIL_SLEEP);

			return false;
		}
		$encryptedEmail = $this->encryptString($email);
		$this->failedLoginCount = $this->db->getFailedLoginCountByEmail($encryptedEmail);
		if ($this->failedLoginCount >= static::MAX_LOGIN_ATTEMPTS) {
			$this->error = 'User account is suspended';
			$this->sleep(static::FAIL_SLEEP);
		} elseif ($failedByIp > 1 && !$this->checkCaptcha()) {
			$this->error = $this->captcha->getErrMsg();
			$this->sleep(static::FAIL_SLEEP);
		} else {
			$user = $this->db->getUserByEmail($encryptedEmail);
			if ($user) {
				if (!password_verify($password, $user['password'])) {
					$this->loginFailed($encryptedEmail, 'pass', $user['id']);
				} elseif (!$this->extraLoginUserCheck($user)) {
					if (!$this->error) {
						$this->error = 'Extra check failed';
					}
					$this->loginFailed($encryptedEmail, 'extra', $user['id']);
				} elseif ($user['active']) {
					$this->db->logUserLogin($user['id'], $this->encryptString($this->ip), $this->encryptString($this->userAgent), 'log');
					unset($user['password']);
					$this->initUser($user, 'log');
					$this->db->removeUserFailedLoginByEmail($encryptedEmail);
					if ($rememberMe) {
						$this->rememberMe($encryptedEmail);
					}

					return true;
				} else {
					$this->error = 'User account is suspended';
					$this->sleep(static::FAIL_SLEEP);
				}
			} else {
				$this->loginFailed($encryptedEmail, 'email');
			}
		}

		return false;
	}

	protected function extraLoginUserCheck(array $user): bool
	{
		return true;
	}

	protected function rememberMe(string $encryptedEmail): void
	{
		$newToken = $this->generateNewToken(static::REMEMBER_LENGTH);
		$this->db->insertRememberMeToken($encryptedEmail, $newToken, $this->encryptString($this->ip), $this->encryptString($this->userAgent));
		$this->setRememberTokenCookie($newToken);
	}

	public function checkLoggedUser(bool $checkActivity = true): bool
	{
		if ($this->isLogged()) {
			$res = $_SESSION['user']['ip'] === $this->ip && $_SESSION['user']['ua'] === $this->userAgent;
			if ($res && $checkActivity) {
				$user = $this->db->getUserByEmail($this->encryptString($this->getUserEmail()));

				return $user['active'];
			}

			return $res;
		}

		return true;
	}

	/**
	 * Automatically login user by remember me token
	 *
	 * @return bool
	 */
	public function loginByRememberMeToken(): bool
	{
		if (!$this->isLogged() && isset($_COOKIE[static::REMEMBER_COOKIE_NAME])) {
			$token = $_COOKIE[static::REMEMBER_COOKIE_NAME];
			if (strlen($token) / 2 !== static::REMEMBER_LENGTH) {
				$this->removeRememberTokenCookie();
			} else {
				$token = $this->db->getRememberMeToken($token);
				if ($token && $token['expire_ts'] > time() && $this->isValidTokenUA($token['ua'])) {
					$user = $this->db->getUserByEmail($token['email']);
					if ($user) {
						if ($user['active']) {
							$this->db->logUserLogin($user['id'], $this->encryptString($this->ip), $this->encryptString($this->userAgent), 'rem');
							$this->initUser($user, 'rem');
							$newToken = $this->generateNewToken(static::REMEMBER_LENGTH);
							$this->db->updateRememberMeToken($token['id'], $newToken, $this->encryptString($this->ip), $this->encryptString($this->userAgent));
							$this->setRememberTokenCookie($newToken);

							return true;
						} else {
							$this->db->removeRememberMeTokenByEmail($token['email']);
							$this->removeRememberTokenCookie();
						}
					} else {
						$this->db->removeRememberMeTokenByEmail($token['email']);
						$this->removeRememberTokenCookie();
					}
				} else {
					if ($token) {
						$this->db->removeRememberMeTokenByEmail($token['email']);
					}
					$this->removeRememberTokenCookie();
				}
			}
		}

		return false;
	}

	private function isValidTokenUA(string $encryptedTokenUserAgent): bool
	{
		if (!static::REMEMBER_UA_MIN_SIMILARITY) {
			return true;
		}
		$percent = 0;
		similar_text($this->decryptString($encryptedTokenUserAgent), $this->userAgent, $percent);

		return $percent >= static::REMEMBER_UA_MIN_SIMILARITY;
	}

	/**
	 * Auto login user by ip - you must know what you are doing when using this
	 *
	 * @return bool
	 */
	public function loginByIp(): bool
	{
		if (!$this->isLogged()) {
			$user = $this->db->getUserByIp($this->encryptString($this->ip));
			if ($user) {
				if ($user['active']) {
					$this->db->logUserLogin($user['id'], $this->encryptString($this->ip), $this->encryptString($this->userAgent), 'ip');
					$this->initUser($user, 'ip');

					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Logout logged user
	 *
	 * @return bool
	 */
	public function logout(): bool
	{
		if ($this->isLogged()) {
			$this->db->removeRememberMeTokenByEmail($this->encryptString($this->getUserEmail()));
			$this->removeRememberTokenCookie();
			unset($_SESSION['user']);
			if (session_id()) {
				session_destroy();
			}

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Register new user
	 *
	 * @param string $email
	 * @param string $password
	 * @param string $password2      Password confirmation
	 * @param bool   $loginOnSuccess Automatically logged in user after successful registration
	 * @param array  $additionalUserData
	 *
	 * @return bool
	 */

	public function register(string $email, string $password, string $password2, bool $loginOnSuccess, array $additionalUserData = []): bool
	{
		if ($password !== $password2) {
			$this->error = 'Passwords do not match';
		} elseif (strlen($password) < static::PASSWORD_MIN_LENGTH) {
			$this->error = 'Password must have at least ' . static::PASSWORD_MIN_LENGTH . ' characters';
		} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->error = 'Email address is not valid';
		} elseif (!$this->checkCaptcha()) {
			$this->error = $this->captcha->getErrMsg();
		} else {
			$encryptedEmail = $this->encryptString($email);
			$user = $this->db->getUserByEmail($encryptedEmail);
			if (!$user) {
				$userId = $this->db->insertNewUser($encryptedEmail, password_hash($password, static::PASSWORD_ALGORITHM, static::PASSWORD_OPTIONS), true, $additionalUserData);
				if ($loginOnSuccess) {
					$user = ['id' => $userId, 'email' => $encryptedEmail, 'created_at' => date('Y-m-d H:i:s')];
					$this->db->logUserLogin($userId, $this->encryptString($this->ip), $this->encryptString($this->userAgent), 'reg');
					$this->initUser($user, 'reg');
				}

				return true;
			} else {
				$this->db->logFailedUserRegister($encryptedEmail, $this->encryptString($this->ip), $this->encryptString($this->userAgent));
				$this->error = 'Email already exists';
				$this->sleep(static::FAIL_SLEEP);
			}
		}

		return false;
	}

	/**
	 * Create recovery password token
	 *
	 * @param string $email
	 *
	 * @return string - Recovery token
	 */
	public function recoverPassword(string $email): string
	{
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->error = 'Email address is not valid';
		} elseif (!$this->checkCaptcha()) {
			$this->error = $this->captcha->getErrMsg();
		} else {
			$encryptedEmail = $this->encryptString($email);
			$user = $this->db->getUserByEmail($encryptedEmail);
			if ($user) {
				if ($user['active']) {
					$token = $this->db->getRecoverTokenByEmail($encryptedEmail);
					if ($token && $token['expire_ts'] > time()) {
						$this->error = 'Recovery email already sent';
						$this->sleep(static::FAIL_SLEEP);
					} else {
						$newToken = $this->generateNewToken(static::RECOVER_LENGTH);
						$expireTs = time() + static::RECOVER_EXPIRE;
						if ($token) {
							$this->db->updateRecoverToken($token['id'], $newToken, $this->encryptString($this->ip), $this->encryptString($this->userAgent), $expireTs);
						} else {
							$this->db->insertRecoverToken($encryptedEmail, $newToken, $this->encryptString($this->ip), $this->encryptString($this->userAgent), $expireTs);
						}

						return $newToken;
					}
				} else {
					$this->db->logFailedUserRecover($encryptedEmail, $this->encryptString($this->ip), $this->encryptString($this->userAgent));
					$this->error = 'User account is suspended';
					$this->sleep(static::FAIL_SLEEP);
				}
			} else {
				$this->db->logFailedUserRecover($encryptedEmail, $this->encryptString($this->ip), $this->encryptString($this->userAgent));
				$this->error = 'Email not exists';
				$this->sleep(static::FAIL_SLEEP);
			}
		}

		return '';
	}

	/**
	 * @param string $token
	 *
	 * @return string - Returns email for reset password token
	 */
	public function getEmailForResetToken(string $token): string
	{
		if (strlen($token) / 2 !== static::RECOVER_LENGTH) {
			$this->error = 'Invalid password recovery token';
			$this->sleep(static::FAIL_SLEEP);
		} else {
			$token = $this->db->getRecoverToken($token);
			if ($token) {
				if ($token['expire_ts'] > time()) {
					return $this->decryptString($token['email']);
				} else {
					$this->error = 'Password recovery token is expired';
				}
			} else {
				$this->error = 'Invalid password recovery token';
				$this->sleep(static::FAIL_SLEEP);
			}
		}

		return '';
	}

	/**
	 * Update user password via password recovery - user is not logged in
	 *
	 * @param string $email
	 * @param string $password
	 * @param string $password2 Password confirmation
	 *
	 * @return bool
	 */

	public function resetPassword(string $email, string $password, string $password2): bool
	{
		if ($password !== $password2) {
			$this->error = 'Passwords do not match';
		} elseif (strlen($password) < static::PASSWORD_MIN_LENGTH) {
			$this->error = 'Password must have at least ' . static::PASSWORD_MIN_LENGTH . ' characters';
		} else {
			$encryptedEmail = $this->encryptString($email);
			$user = $this->db->getUserByEmail($encryptedEmail);
			if ($user) {
				$this->db->updateUserPassword($user['id'], password_hash($password, static::PASSWORD_ALGORITHM, static::PASSWORD_OPTIONS));
				$this->db->removeRecoverTokenByEmail($encryptedEmail);

				return true;
			} else {
				$this->error = 'Unknown user for this recovery token';
				$this->sleep(static::FAIL_SLEEP);
			}
		}

		return false;
	}

	/**
	 * Update user password by user itself - user is logged in
	 *
	 * @param string  $password
	 * @param string  $password2   Password confirmation
	 * @param ?string $oldPassword Old password if needed - it is needed, when user was logged in via remember me token
	 *
	 * @return bool
	 */
	public function changePassword(string $password, string $password2, ?string $oldPassword): bool
	{
		$oldReq = $this->isOldUserPassRequired();
		if ($oldReq && !$oldPassword) {
			$this->error = 'Wrong old password';
		} elseif ($password !== $password2) {
			$this->error = 'Passwords do not match';
		} elseif (strlen($password) < static::PASSWORD_MIN_LENGTH) {
			$this->error = 'Password must have at least ' . static::PASSWORD_MIN_LENGTH . ' characters';
		} else {
			$currentPassword = $this->db->getUserPassword($this->getUserId());
			if ($oldReq && !password_verify($oldPassword, $currentPassword)) {
				$this->error = 'Wrong old password';
			} else {
				$this->db->updateUserPassword($this->getUserId(), password_hash($password, static::PASSWORD_ALGORITHM, static::PASSWORD_OPTIONS));

				return true;
			}
		}

		return false;
	}

	private function generateNewToken(int $length): string
	{
		return bin2hex(random_bytes($length));
	}

	private function setRememberTokenCookie(string $token): void
	{
		$this->setCookie(static::REMEMBER_COOKIE_NAME, $token, time() + static::REMEMBER_EXPIRE * 86400, '/', '', true, true);
	}

	private function removeRememberTokenCookie(): void
	{
		$this->setCookie(static::REMEMBER_COOKIE_NAME);
	}

	private function loginFailed(string $encryptedEmail, string $failedLoginMsg, string $id = null): void
	{
		if ($this->failedLoginCount + 1 >= static::MAX_LOGIN_ATTEMPTS) {
			$this->logFailedLogin($encryptedEmail, 'suspend');
			if ($id) {
				$this->db->deactivateUser($id);
			}
			$this->error = 'Too many failed login tries - user account was suspended';
		} else {
			$this->logFailedLogin($encryptedEmail, $failedLoginMsg);
			if (!$this->error) {
				$this->error = 'Wrong user or password';
			}
		}
		$this->failedLoginCount++;
		$this->sleep($this->failedLoginCount * static::FAILED_LOGIN_DELAY_MULTIPLIER);
	}

	private function logFailedLogin(string $encryptedEmail, string $msg): void
	{
		$this->db->logFailedUserLogin($encryptedEmail, $this->encryptString($this->ip), $this->encryptString($this->userAgent), $msg);
	}

	private function checkCaptcha(): bool
	{
		if (!$this->captcha->isActive()) {

			return true;
		}

		return $this->captcha->isValid();
	}

	protected function initUser($user, string $via): void
	{
		unset($user['password']);
		$_SESSION['user'] = $user;
		$_SESSION['user']['email'] = $this->decryptString($user['email']);
		$_SESSION['user']['via'] = $via;
		$_SESSION['user']['ip'] = $this->ip;
		$_SESSION['user']['ua'] = $this->userAgent;
	}

	protected function encryptString(string $string): string
	{
		return $string ? base64_encode(openssl_encrypt($string, static::CIPHER, $this->cipherKey, OPENSSL_RAW_DATA, $this->cipherIv)) : '';
	}

	protected function decryptString(string $string): string
	{
		return $string ? openssl_decrypt(base64_decode($string), static::CIPHER, $this->cipherKey, OPENSSL_RAW_DATA, $this->cipherIv) : '';
	}

	public function getError(): string
	{
		return $this->error;
	}

	public function getFailedLoginCount(): int
	{
		return $this->failedLoginCount;
	}

	public function getUserId(): string
	{
		return $this->isLogged() ? $_SESSION['user']['id'] : '';
	}

	public function getUserEmail(): string
	{
		return $this->isLogged() ? $_SESSION['user']['email'] : '';
	}

	public function getRegistrationTime(): string
	{
		return $this->isLogged() ? $_SESSION['user']['created_at'] : '';
	}

	public function isOldUserPassRequired(): bool
	{
		return $this->isLogged() && $_SESSION['user']['via'] !== 'log';
	}

	public function isLoggedViaLogin(): bool
	{
		return $this->isLogged() && $_SESSION['user']['via'] === 'log';
	}

	public function isLoggedViaIp(): bool
	{
		return $this->isLogged() && $_SESSION['user']['via'] === 'ip';
	}

	/**
	 * @codeCoverageIgnore
	 */
	public function setCookie(string $name, string $value = '', int $expire = 0, string $path = '', string $domain = '', bool $secure = false, bool $httpOnly = false): void
	{
		setcookie($name, $value, $expire, $path, $domain, $secure, $httpOnly);
	}

	public function setCipherKey(string $cipherKey): void
	{
		$this->cipherKey = $cipherKey;
	}

	public function setCipherIv(string $cipherIv): void
	{
		$this->cipherIv = $cipherIv;
	}

	/**
	 * @codeCoverageIgnore
	 */
	public function sleep(int $seconds): void
	{
		sleep($seconds);
	}
}