<?php

namespace Softko\Db;

interface UserDbInterface
{
	/**
	 * @param string $email
	 *
	 * @return array - minimal keys 'id', 'email', 'active', 'password'
	 */
	function getUserByEmail(string $email): array;

	/**
	 * Used for auto login user by ip - you must know what you are doing when using this
	 *
	 * @param string $ip
	 *
	 * @return array - minimal keys 'id', 'email', 'active'
	 */
	function getUserByIp(string $ip): array;

	function getUserPassword(string $id): string;

	function deactivateUser(string $id): void;

	function updateUserPassword(string $id, string $password): void;

	function logUserLogin(string $userId, string $ip, string $userAgent, string $via): void;

	function logFailedUserLogin(string $email, string $ip, string $userAgent, string $msg): void;

	function removeUserFailedLoginByEmail(string $email);

	/**
	 * Should be implemented item expiration - see MysqlDbUser.php for example
	 *
	 * @param string $ip
	 *
	 * @return int
	 */
	function getFailedLoginCountByIp(string $ip): int;

	function getFailedLoginCountByEmail(string $email): int;

	/**
	 * @param string $token
	 *
	 * @return array - minimal keys 'id', 'email', 'ua' (user_agent), 'expire_ts' (expire timestamp in seconds)
	 */
	function getRememberMeToken(string $token): array;

	/**
	 * Should update 'expire_ts' (expire timestamp in seconds)
	 *
	 * @param string $email
	 * @param string $token
	 * @param string $ip
	 * @param string $userAgent
	 */
	function insertRememberMeToken(string $email, string $token, string $ip, string $userAgent): void;

	/**
	 * Should update 'expire_ts' (expire timestamp in seconds)
	 *
	 * @param string $id
	 * @param string $token
	 * @param string $ip
	 * @param string $userAgent
	 */
	function updateRememberMeToken(string $id, string $token, string $ip, string $userAgent): void;

	function removeRememberMeTokenByEmail(string $email): void;

	function insertNewUser(string $email, string $password, bool $active, array $additionalData = []): string;

	function logFailedUserRegister(string $email, string $ip, string $userAgent): void;

	/**
	 * @param string $token
	 *
	 * @return array - minimal keys 'email', 'expire_ts' (expire timestamp in seconds)
	 */
	function getRecoverToken(string $token): array;

	/**
	 * @param string $email
	 *
	 * @return array - minimal keys 'id', 'expire_ts' (expire timestamp in seconds)
	 */
	function getRecoverTokenByEmail(string $email): array;

	/**
	 * Should update 'expire_ts' (expire timestamp in seconds)
	 *
	 * @param string $email
	 * @param string $token
	 * @param string $ip
	 * @param string $userAgent
	 * @param int $expireTimestamp
	 */
	function insertRecoverToken(string $email, string $token, string $ip, string $userAgent, int $expireTimestamp): void;

	/**
	 * Should update 'expire_ts' (expire timestamp in seconds)
	 *
	 * @param string $id
	 * @param string $token
	 * @param string $ip
	 * @param string $userAgent
	 * @param int $expireTimestamp
	 */
	function updateRecoverToken(string $id, string $token, string $ip, string $userAgent, int $expireTimestamp): void;

	function removeRecoverTokenByEmail(string $email): void;

	function logFailedUserRecover(string $email, string $ip, string $userAgent): void;

	function isIpBlacklisted(string $ip): bool;
}