<?php

namespace Softko\Db;

use Softko\User\User;

class PdoDb implements UserDbInterface
{
	protected \PDO $pdo;

	function __construct(\PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	public function getUserByEmail(string $email): array
	{
		$stmt = $this->pdo->prepare('SELECT id,email,active,password FROM app_user WHERE email=?');
		$stmt->bindValue(1, $email, \PDO::PARAM_STR);
		$stmt->execute();
		$user = $stmt->fetch(\PDO::FETCH_ASSOC);

		return $user ?: [];
	}

	function getUserByIp(string $ip): array
	{
		$stmt = $this->pdo->prepare('SELECT u.id,u.email,u.active,u.password FROM app_user_ip_login l INNER JOIN app_user u ON u.id=l.user_id WHERE l.ip=?');
		$stmt->bindValue(1, $ip, \PDO::PARAM_STR);
		$stmt->execute();
		$user = $stmt->fetch(\PDO::FETCH_ASSOC);

		return $user ?: [];
	}

	function getUserPassword(string $id): string
	{
		$stmt = $this->pdo->prepare('SELECT password FROM app_user WHERE id=?');
		$stmt->bindValue(1, $id, \PDO::PARAM_STR);
		$stmt->execute();
		$res = $stmt->fetchColumn();
		return $res ?: '';
	}

	function deactivateUser(string $id): void
	{
		$stmt = $this->pdo->prepare('UPDATE app_user SET active=0 WHERE id=?');
		$stmt->bindValue(1, $id, \PDO::PARAM_STR);
		$stmt->execute();
	}

	function updateUserPassword(string $id, string $password): void
	{
		$stmt = $this->pdo->prepare('UPDATE app_user SET password=? WHERE id=?');
		$stmt->bindValue(1, $password, \PDO::PARAM_STR);
		$stmt->bindValue(2, $id, \PDO::PARAM_STR);
		$stmt->execute();
	}

	public function logUserLogin(string $userId, string $ip, string $userAgent, string $via): void
	{
		$stmt = $this->pdo->prepare('INSERT INTO app_user_login (user_id,ip,ua,via) VALUES (?,?,?,?)');
		$stmt->bindValue(1, $userId, \PDO::PARAM_STR);
		$stmt->bindValue(2, $ip, \PDO::PARAM_STR);
		$stmt->bindValue(3, $userAgent, \PDO::PARAM_STR);
		$stmt->bindValue(4, $via, \PDO::PARAM_STR);
		$stmt->execute();
	}

	public function logFailedUserLogin(string $email, string $ip, string $userAgent, string $msg): void
	{
		$stmt = $this->pdo->prepare('INSERT INTO app_user_login_failed (email,ip,ua,msg) VALUES (?,?,?,?)');
		$stmt->bindValue(1, $email, \PDO::PARAM_STR);
		$stmt->bindValue(2, $ip, \PDO::PARAM_STR);
		$stmt->bindValue(3, $userAgent, \PDO::PARAM_STR);
		$stmt->bindValue(4, $msg, \PDO::PARAM_STR);
		$stmt->execute();
	}

	public function removeUserFailedLoginByEmail(string $email): void
	{
		$stmt = $this->pdo->prepare('SELECT id FROM app_user_login_failed WHERE email=?');
		$stmt->bindValue(1, $email, \PDO::PARAM_STR);
		$stmt->execute();
		$failed = $stmt->fetchAll(\PDO::FETCH_COLUMN);
		if ($failed) {
			$idsString = implode(',', array_values($failed));
			$stmt = $this->pdo->prepare('INSERT INTO app_user_login_failed_history (email,ip,ua,msg,created_at) SELECT email,ip,ua,msg,created_at FROM app_user_login_failed WHERE id IN(' . $idsString . ')');
			$stmt->execute();
			$stmt = $this->pdo->prepare('DELETE FROM app_user_login_failed WHERE id IN(' . $idsString . ')');
			$stmt->execute();
		}
	}

	public function getFailedLoginCountByIp(string $ip): int
	{
		$stmt = $this->pdo->prepare('SELECT COUNT(id) FROM app_user_login_failed WHERE ip=? AND created_at>DATE_SUB(NOW(), INTERVAL 1 DAY)');
		$stmt->bindValue(1, $ip, \PDO::PARAM_STR);
		$stmt->execute();
		$res = $stmt->fetchColumn();
		return $res ? (int)$res : 0;
	}

	public function getFailedLoginCountByEmail(string $email): int
	{
		$stmt = $this->pdo->prepare('SELECT COUNT(id) FROM app_user_login_failed WHERE email=?');
		$stmt->bindValue(1, $email, \PDO::PARAM_STR);
		$stmt->execute();
		$res = $stmt->fetchColumn();
		return $res ? (int)$res : 0;
	}

	public function getRememberMeToken(string $token): array
	{
		$stmt = $this->pdo->prepare('SELECT id,email,ua,expire_ts FROM app_user_remember WHERE token=?');
		$stmt->bindValue(1, $token, \PDO::PARAM_STR);
		$stmt->execute();
		$remember = $stmt->fetch(\PDO::FETCH_ASSOC);

		return $remember ? $remember : [];
	}

	public function insertRememberMeToken(string $email, string $token, string $ip, string $userAgent): void
	{
		$stmt = $this->pdo->prepare('INSERT INTO app_user_remember (token,email,ip,ua,expire_ts) VALUES (?,?,?,?,?)');
		$stmt->bindValue(1, $token, \PDO::PARAM_STR);
		$stmt->bindValue(2, $email, \PDO::PARAM_STR);
		$stmt->bindValue(3, $ip, \PDO::PARAM_STR);
		$stmt->bindValue(4, $userAgent, \PDO::PARAM_STR);
		$stmt->bindValue(5, time() + User::REMEMBER_EXPIRE * 86400, \PDO::PARAM_INT);
		$stmt->execute();
	}

	public function updateRememberMeToken(string $id, string $token, string $ip, string $userAgent): void
	{
		$stmt = $this->pdo->prepare('UPDATE app_user_remember SET token=?,ip=?,ua=?,expire_ts=? WHERE id=?');
		$stmt->bindValue(1, $token, \PDO::PARAM_STR);
		$stmt->bindValue(2, $ip, \PDO::PARAM_STR);
		$stmt->bindValue(3, $userAgent, \PDO::PARAM_STR);
		$stmt->bindValue(4, time() + User::REMEMBER_EXPIRE * 86400, \PDO::PARAM_INT);
		$stmt->bindValue(5, $id, \PDO::PARAM_STR);
		$stmt->execute();
	}

	public function removeRememberMeTokenByEmail(string $email): void
	{
		$stmt = $this->pdo->prepare('DELETE FROM app_user_remember WHERE email=?');
		$stmt->bindValue(1, $email, \PDO::PARAM_STR);
		$stmt->execute();
	}

	function insertNewUser(string $email, string $password, bool $active, array $additionalData = []): string
	{
		$additionalData['email'] = $email;
		$additionalData['password'] = $password;
		$additionalData['active'] = $active;
		$cols = [];
		$qMarks = [];
		foreach ($additionalData as $k => $v) {
			$cols[] = $k;
			$qMarks[] = '?';
		}
		$stmt = $this->pdo->prepare('INSERT INTO app_user (' . implode(',', $cols) . ') VALUES (' . implode(',', $qMarks) . ')');
		$i = 1;
		foreach ($additionalData as $k => $v) {
			$stmt->bindValue($i++, $v, \PDO::PARAM_STR);
		}
		$stmt->execute();

		return $this->pdo->lastInsertId();
	}

	function logFailedUserRegister(string $email, string $ip, string $userAgent): void
	{
		$stmt = $this->pdo->prepare('INSERT INTO app_user_reg_failed (ip,ua,email) VALUES (?,?,?)');
		$stmt->bindValue(1, $ip, \PDO::PARAM_STR);
		$stmt->bindValue(2, $userAgent, \PDO::PARAM_STR);
		$stmt->bindValue(3, $email, \PDO::PARAM_STR);
		$stmt->execute();
	}

	function getRecoverToken(string $token): array
	{
		$stmt = $this->pdo->prepare('SELECT email,expire_ts FROM app_user_recover WHERE token=?');
		$stmt->bindValue(1, $token, \PDO::PARAM_STR);
		$stmt->execute();
		$recover = $stmt->fetch(\PDO::FETCH_ASSOC);

		return $recover ? $recover : [];
	}

	function getRecoverTokenByEmail(string $email): array
	{
		$stmt = $this->pdo->prepare('SELECT id,expire_ts FROM app_user_recover WHERE email=?');
		$stmt->bindValue(1, $email, \PDO::PARAM_STR);
		$stmt->execute();
		$recover = $stmt->fetch(\PDO::FETCH_ASSOC);

		return $recover ? $recover : [];
	}

	function insertRecoverToken(string $email, string $token, string $ip, string $userAgent, int $expireTimestamp): void
	{
		$stmt = $this->pdo->prepare('INSERT INTO app_user_recover (token,email,ip,ua,expire_ts) VALUES (?,?,?,?,?)');
		$stmt->bindValue(1, $token, \PDO::PARAM_STR);
		$stmt->bindValue(2, $email, \PDO::PARAM_STR);
		$stmt->bindValue(3, $ip, \PDO::PARAM_STR);
		$stmt->bindValue(4, $userAgent, \PDO::PARAM_STR);
		$stmt->bindValue(5, $expireTimestamp, \PDO::PARAM_INT);
		$stmt->execute();
	}

	function updateRecoverToken(string $id, string $token, string $ip, string $userAgent, int $expireTimestamp): void
	{
		$stmt = $this->pdo->prepare('UPDATE app_user_recover SET token=?,ip=?,ua=?,expire_ts=? WHERE id=?');
		$stmt->bindValue(1, $token, \PDO::PARAM_STR);
		$stmt->bindValue(2, $ip, \PDO::PARAM_STR);
		$stmt->bindValue(3, $userAgent, \PDO::PARAM_STR);
		$stmt->bindValue(4, $expireTimestamp, \PDO::PARAM_INT);
		$stmt->bindValue(5, $id, \PDO::PARAM_STR);
		$stmt->execute();
	}

	function removeRecoverTokenByEmail(string $email): void
	{
		$stmt = $this->pdo->prepare('DELETE FROM app_user_recover WHERE email=?');
		$stmt->bindValue(1, $email, \PDO::PARAM_STR);
		$stmt->execute();
	}

	function logFailedUserRecover(string $email, string $ip, string $userAgent): void
	{
		$stmt = $this->pdo->prepare('INSERT INTO app_user_rec_failed (ip,ua,email) VALUES (?,?,?)');
		$stmt->bindValue(1, $ip, \PDO::PARAM_STR);
		$stmt->bindValue(2, $userAgent, \PDO::PARAM_STR);
		$stmt->bindValue(3, $email, \PDO::PARAM_STR);
		$stmt->execute();
	}

	public function isIpBlacklisted(string $ip): bool
	{
		$stmt = $this->pdo->prepare('SELECT id FROM black_ip WHERE ip=?');
		$stmt->bindValue(1, $ip, \PDO::PARAM_STR);
		$stmt->execute();
		$res = $stmt->fetchColumn();
		return $res ? true : false;
	}
}