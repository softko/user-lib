<?php

namespace Softko\Captcha;

interface CaptchaInterface
{
	function isActive(): bool;

	function isValid(): bool;

	function getErrMsg(): string;
}