<?php

namespace Softko\Captcha;

class ReCaptchaV2 implements CaptchaInterface
{
	private array $settings;
	private bool $isProduction;
	private string $errMsg = '';

	public function __construct(array $settings, $isProduction = true)
	{
		$this->settings = $settings;
		$this->isProduction = $isProduction;
	}

	public function isActive(): bool
	{
		return $this->settings['active'] && !(!$this->isProduction && isset($_GET['no_captcha']));
	}

	public function isValid(): bool
	{
		if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response']) {
			$curlData = $this->post(
				$this->settings['url'],
				[
					'secret' => $this->settings['secret'],
					'response' => $_POST['g-recaptcha-response'],
					'remoteip' => $_SERVER['REMOTE_ADDR'],
				]
			);
			if ($curlData['error'] || $curlData['info']['http_code'] != 200) {
				$this->errMsg = 'Captcha communication failed';

				return false;
			} else {
				$res = json_decode($curlData['result'], true);
				if ($res && $res['success']) {
					return true;
				} else {
					$this->errMsg = 'Captcha is not valid';

					return false;
				}
			}
		} else {
			$this->errMsg = 'Please resolve captcha';

			return false;
		}
	}

	public function getErrMsg(): string
	{
		return $this->errMsg;
	}

	private function post(string $url, array $fields): array
	{
		$ch = curl_init($url);
		curl_setopt_array(
			$ch,
			[
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $fields,
				CURLOPT_CONNECTTIMEOUT => 5,
				CURLOPT_TIMEOUT => 60,
				CURLOPT_RETURNTRANSFER => true,
			]
		);
		$result = curl_exec($ch);
		$data = ['result' => $result, 'error' => $result === false ? curl_error($ch) : null, 'info' => curl_getinfo($ch)];
		curl_close($ch);

		return $data;
	}
}