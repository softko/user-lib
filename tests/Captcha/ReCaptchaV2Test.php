<?php

namespace Softko\Captcha;

use PHPUnit\Framework\TestCase;

class ReCaptchaV2Test extends TestCase
{
	public function testIsActive()
	{
		$this->assertTrue($this->getReCaptchaInstance(['active' => true])->isActive());
		$_GET['no_captcha'] = 1;
		$this->assertTrue($this->getReCaptchaInstance(['active' => true])->isActive());
	}

	public function testIsActiveNotActive()
	{
		$this->assertFalse($this->getReCaptchaInstance(['active' => false])->isActive());
		$_GET['no_captcha'] = 1;
		$this->assertFalse($this->getReCaptchaInstance(['active' => true], false)->isActive());
	}

	private function getReCaptchaInstance(array $settings, $isProduction = true)
	{
		return new ReCaptchaV2($settings, $isProduction);
	}
}
