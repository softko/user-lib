<?php

namespace Softko\User;

function session_id()
{
	return true;
}

function session_destroy()
{

}

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
	const ENCRYPTED_EMAIL = 'ZiaIiEsftyRJXXsfhRphDQ==';
	const ENCRYPTED_UA = 'Sd9pp5kJVgug0C/R+RGpVediMcSw2HI9kfmLjcO9kFbIO3DoBnewMI+xuZwx5ISQIyXvrkXX0AS/554QQ9+qTEvVkNPdrXyUSubcKcPfyal3tATh4aYltPtCoPsp1TnJYM4GryMKKZBJgKsVmVm+aA==';
	const ENCRYPTED_UA_SIMILAR = 'XtiowtDuPkru91Q1AG5GwE1MgCY0zR0AKQxjDfYksuQeY/YACSwpuKSeMHyrYbr9dowGXGPvMY4vjgwPHJQRfJWZtggLN9dZ8Dhf/rHe4WCbXzf272cKPi/FtucxhRWUcxRCGvnD/ndBNTDnO9CDww==';
	const ENCRYPTED_IP = '+ru2XCeC0LPe43gXZh7dJQ==';
	const PASS_HASH = '$2y$10$/SIRrPA8c7oozeZ//Jzib.yYJLJJrRDULnJQyTMtNNLlDAJiISsUi';
	/**
	 * @var CustomUser
	 */
	private $user;
	/**
	 * @var MockObject
	 */
	private $dbMock;
	/**
	 * @var MockObject
	 */
	private $captchaMock;

	protected function setUp(): void
	{
		$this->dbMock = $this->createMock('Softko\Db\UserDbInterface');
		$this->captchaMock = $this->createMock('Softko\Captcha\CaptchaInterface');
		$this->user = new CustomUser(
			$this->dbMock,
			$this->captchaMock,
			'138.161.213.58',
			'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36'
		);
		$this->user->setCipherKey('abcdefghijklmnopqrstuvxywz123456');
		$this->user->setCipherIv('abcdefghijklmnop');
	}

	public function testIsLogged()
	{
		$this->assertFalse($this->user->isLogged());
		$_SESSION['user'] = true;
		$this->assertTrue($this->user->isLogged());
		unset($_SESSION['user']);
		$this->assertFalse($this->user->isLogged());
	}

	public function testLoginTooManyFailsFromIp()
	{
		$this->dbMock->expects($this->once())->method('getFailedLoginCountByIp')->with(self::ENCRYPTED_IP)->willReturn(21);
		$this->dbMock->expects($this->never())->method('getUserByEmail');
		$this->assertFalse($this->user->login('', '', false));
		$this->assertSame('Too many failed login tries from your ip address, try again later', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([2], $this->user->getSleepCalls());
		$this->assertSame(0, $this->user->getFailedLoginCount());
	}

	public function testLoginTooManyFailsByEmail()
	{
		$this->dbMock->expects($this->once())->method('getFailedLoginCountByIp')->with(self::ENCRYPTED_IP)->willReturn(20);
		$this->dbMock->expects($this->once())->method('getFailedLoginCountByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(20);
		$this->dbMock->expects($this->never())->method('getUserByEmail');
		$this->assertFalse($this->user->login('my@email.com', '', false));
		$this->assertSame('User account is suspended', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([2], $this->user->getSleepCalls());
		$this->assertSame(20, $this->user->getFailedLoginCount());
	}

	public function testLoginUnknownUser()
	{
		$this->dbMock->expects($this->once())->method('getFailedLoginCountByIp')->willReturn(1);
		$this->captchaMock->expects($this->never())->method($this->anything());
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn([]);
		$this->dbMock->expects($this->once())->method('logFailedUserLogin')->with(self::ENCRYPTED_EMAIL, self::ENCRYPTED_IP, self::ENCRYPTED_UA, 'email');
		$this->dbMock->expects($this->never())->method('deactivateUser');
		$this->assertFalse($this->user->login('my@email.com', '', false));
		$this->assertSame('Wrong user or password', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([11], $this->user->getSleepCalls());
	}

	public function testLoginPasswordNotMatch()
	{
		$this->captchaMock->expects($this->never())->method($this->anything());
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['id' => 7, 'active' => 1, 'password' => self::PASS_HASH]);
		$this->dbMock->expects($this->once())->method('logFailedUserLogin')->with(self::ENCRYPTED_EMAIL, self::ENCRYPTED_IP, self::ENCRYPTED_UA, 'pass');
		$this->assertFalse($this->user->login('my@email.com', 'asdqw', false));
		$this->assertSame('Wrong user or password', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([11], $this->user->getSleepCalls());
	}

	public function testLoginPasswordNotMatchSuspendAccount()
	{
		$this->dbMock->expects($this->once())->method('getFailedLoginCountByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(19);
		$this->captchaMock->expects($this->never())->method($this->anything());
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['id' => 7, 'active' => 1, 'password' => self::PASS_HASH]);
		$this->dbMock->expects($this->once())->method('logFailedUserLogin')->with(self::ENCRYPTED_EMAIL, self::ENCRYPTED_IP, self::ENCRYPTED_UA, 'suspend');
		$this->dbMock->expects($this->once())->method('deactivateUser')->with(7);
		$this->dbMock->expects($this->never())->method('removeUserFailedLoginByEmail');
		$this->assertFalse($this->user->login('my@email.com', 'asdqw', false));
		$this->assertSame('Too many failed login tries - user account was suspended', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([220], $this->user->getSleepCalls());
	}

	public function testLoginAccountNotActive()
	{
		$this->captchaMock->expects($this->never())->method($this->anything());
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['active' => 0, 'password' => self::PASS_HASH]);
		$this->dbMock->expects($this->never())->method('logFailedUserLogin');
		$this->dbMock->expects($this->never())->method('removeUserFailedLoginByEmail');
		$this->assertFalse($this->user->login('my@email.com', 'asdqwe', false));
		$this->assertSame('User account is suspended', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([2], $this->user->getSleepCalls());
	}

	public function testLoginAccountExtraUserFailed()
	{
		$this->user = new CustomUserExtraLoginCheckFalse(
			$this->dbMock,
			$this->captchaMock,
			'172.0.0.1',
			'my ua'
		);
		$this->user->setCipherKey('abcdefghijklmnopqrstuvxywz123456');
		$this->user->setCipherIv('abcdefghijklmnop');
		$this->captchaMock->expects($this->never())->method($this->anything());
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['id' => 7, 'active' => 1, 'password' => self::PASS_HASH]);
		$this->dbMock->expects($this->once())->method('logFailedUserLogin')->with(self::ENCRYPTED_EMAIL, 'Z2E3NohGVj3pPVELSMCbNQ==', 'fiwIwmlNB81VHnBnIZusOQ==', 'extra');
		$this->dbMock->expects($this->never())->method('removeUserFailedLoginByEmail');
		$this->assertFalse($this->user->login('my@email.com', 'asdqwe', false));
		$this->assertSame('Extra check failed', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([11], $this->user->getSleepCalls());
	}

	public function testLoginWrongCaptcha()
	{
		$this->dbMock->expects($this->once())->method('getFailedLoginCountByIp')->willReturn(2);
		$this->captchaMock->expects($this->once())->method('isActive')->willReturn(true);
		$this->captchaMock->expects($this->once())->method('isValid')->willReturn(false);
		$this->captchaMock->expects($this->once())->method('getErrMsg')->willReturn('captcha error');
		$this->dbMock->expects($this->never())->method('getUserByEmail');
		$this->dbMock->expects($this->never())->method('removeUserFailedLoginByEmail');
		$this->assertFalse($this->user->login('my@email.com', '', false));
		$this->assertSame('captcha error', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([2], $this->user->getSleepCalls());
	}

	public function testLoginCaptchaNotActive()
	{
		$this->dbMock->expects($this->once())->method('getFailedLoginCountByIp')->willReturn(2);
		$this->captchaMock->expects($this->once())->method('isActive')->willReturn(false);
		$this->captchaMock->expects($this->never())->method('isValid');
		$this->captchaMock->expects($this->never())->method('getErrMsg');
		$this->dbMock->expects($this->once())->method('getUserByEmail');
		$this->dbMock->expects($this->never())->method('removeUserFailedLoginByEmail');
		$this->assertFalse($this->user->login('my@email.com', '', false));
		$this->assertSame('Wrong user or password', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([11], $this->user->getSleepCalls());
	}

	public function testLoginSuccessNoRemember()
	{
		$this->assertArrayNotHasKey('user', $_SESSION);
		$this->captchaMock->expects($this->never())->method($this->anything());
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(
			['id' => '6', 'active' => 1, 'password' => self::PASS_HASH, 'email' => self::ENCRYPTED_EMAIL]
		);
		$this->dbMock->expects($this->never())->method('logFailedUserLogin');
		$this->dbMock->expects($this->never())->method('insertRememberMeToken');
		$this->dbMock->expects($this->once())->method('logUserLogin')->with('6', self::ENCRYPTED_IP, self::ENCRYPTED_UA, 'log');
		$this->dbMock->expects($this->once())->method('removeUserFailedLoginByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->assertTrue($this->user->login('my@email.com', 'asdqwe', false));
		$this->assertSame('', $this->user->getError());
		$this->assertArrayHasKey('user', $_SESSION);
		$this->assertArrayNotHasKey('password', $_SESSION['user']);
		$this->assertArrayHasKey('id', $_SESSION['user']);
		$this->assertArrayHasKey('active', $_SESSION['user']);
		$this->assertArrayHasKey('email', $_SESSION['user']);
		$this->assertArrayHasKey('via', $_SESSION['user']);
		$this->assertArrayHasKey('ip', $_SESSION['user']);
		$this->assertArrayHasKey('ua', $_SESSION['user']);
		$this->assertSame('6', $_SESSION['user']['id']);
		$this->assertSame('my@email.com', $_SESSION['user']['email']);
		$this->assertSame('log', $_SESSION['user']['via']);
		$this->assertSame('138.161.213.58', $_SESSION['user']['ip']);
		$this->assertSame('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', $_SESSION['user']['ua']);
		unset($_SESSION['user']);
		$this->assertCount(0, $this->user->getSleepCalls());

	}

	public function testLoginSuccessWithRemember()
	{
		$this->assertArrayNotHasKey('user', $_SESSION);
		$this->captchaMock->expects($this->never())->method($this->anything());
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(
			['id' => '9', 'active' => 1, 'password' => self::PASS_HASH, 'email' => self::ENCRYPTED_EMAIL, 'created_at' => 1]
		);
		$this->dbMock->expects($this->never())->method('logFailedUserLogin');
		$this->dbMock->expects($this->once())->method('logUserLogin')->with(
			'9',
			self::ENCRYPTED_IP,
			self::ENCRYPTED_UA,
			'log'
		);
		$this->dbMock->expects($this->once())->method('removeUserFailedLoginByEmail')->with(self::ENCRYPTED_EMAIL);
		$rememberToken = '';
		$this->dbMock->expects($this->once())->method('insertRememberMeToken')->with(
			self::ENCRYPTED_EMAIL,
			$this->callback(
				function ($token) use (&$rememberToken) {
					$rememberToken = $token;

					return strlen($token) === 56;
				}
			),
			self::ENCRYPTED_IP,
			self::ENCRYPTED_UA
		);
		$this->assertTrue($this->user->login('my@email.com', 'asdqwe', true));
		$this->assertCount(1, $this->user->getSetCookieCalls());
		$this->assertEquals([['Remember2', $rememberToken, time() + 86400 * 2, '/', '', true, true]], $this->user->getSetCookieCalls());
		$this->assertSame('', $this->user->getError());
		$this->assertArrayHasKey('user', $_SESSION);
		$this->assertArrayNotHasKey('password', $_SESSION['user']);
		$this->assertArrayHasKey('id', $_SESSION['user']);
		$this->assertArrayHasKey('active', $_SESSION['user']);
		$this->assertArrayHasKey('email', $_SESSION['user']);
		$this->assertArrayHasKey('via', $_SESSION['user']);
		$this->assertArrayHasKey('created_at', $_SESSION['user']);
		$this->assertArrayHasKey('ip', $_SESSION['user']);
		$this->assertArrayHasKey('ua', $_SESSION['user']);
		$this->assertSame('9', $_SESSION['user']['id']);
		$this->assertSame('my@email.com', $_SESSION['user']['email']);
		$this->assertSame('log', $_SESSION['user']['via']);
		$this->assertSame('138.161.213.58', $_SESSION['user']['ip']);
		$this->assertSame('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', $_SESSION['user']['ua']);
		unset($_SESSION['user']);
	}

	public function testCheckLoggedUserNoActivity()
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$_SESSION['user']['ip'] = 'ip';
		$_SESSION['user']['ua'] = 'ua';
		$this->assertFalse($this->user->checkLoggedUser(false), 'ip');
		$_SESSION['user']['ip'] = '138.161.213.58';
		$this->assertFalse($this->user->checkLoggedUser(false), 'ua');
		$_SESSION['user']['ua'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36';
		$this->assertTrue($this->user->checkLoggedUser(false));
		unset($_SESSION['user']);
	}

	public function testCheckLoggedUserWithActivity()
	{
		$this->dbMock->expects($this->exactly(2))->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['active' => '1']);
		$_SESSION['user']['ip'] = 'ip';
		$_SESSION['user']['ua'] = 'ua';
		$_SESSION['user']['email'] = 'my@email.com';
		$this->assertFalse($this->user->checkLoggedUser(true), 'ip');
		$_SESSION['user']['ip'] = '138.161.213.58';
		$this->assertFalse($this->user->checkLoggedUser(true), 'ua');
		$_SESSION['user']['ua'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36';
		$this->assertTrue($this->user->checkLoggedUser(true));
		$this->assertTrue($this->user->checkLoggedUser());
		unset($_SESSION['user']);
	}

	public function testCheckLoggedUserWithActivityNoActiveUser()
	{
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['active' => '0']);
		$_SESSION['user']['email'] = 'my@email.com';
		$_SESSION['user']['ip'] = '138.161.213.58';
		$_SESSION['user']['ua'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36';
		$this->assertFalse($this->user->checkLoggedUser());
		unset($_SESSION['user']);
	}

	public function testCheckLoggedUserNotLogged()
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->assertTrue($this->user->checkLoggedUser());
	}

	public function testLoginByRememberMeTokenUserLogged()
	{
		$_SESSION['user'] = true;
		$_COOKIE['Remember2'] = true;
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->loginByRememberMeToken());
		$this->assertCount(0, $this->user->getSetCookieCalls());
		unset($_SESSION['user']);
		unset($_COOKIE['Remember2']);
	}

	public function testLoginByRememberMeTokenNoCookie()
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->loginByRememberMeToken());
		$this->assertCount(0, $this->user->getSetCookieCalls());
	}

	public function testLoginByRememberMeTokenWrongToken()
	{
		$_COOKIE['Remember2'] = '1234567890';
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->loginByRememberMeToken());
		$this->assertCount(1, $this->user->getSetCookieCalls());
		$this->assertEquals([['Remember2']], $this->user->getSetCookieCalls());
		unset($_COOKIE['Remember2']);
	}

	public function testLoginByRememberMeTokenUnknownToken()
	{
		$token = '12345678901234567890123456789012345678901234567890123456';
		$_COOKIE['Remember2'] = $token;
		$this->dbMock->expects($this->once())->method('getRememberMeToken')->with($token)->willReturn([]);
		$this->dbMock->expects($this->never())->method('getUserByEmail');
		$this->dbMock->expects($this->never())->method('logUserLogin');
		$this->dbMock->expects($this->never())->method('removeRememberMeTokenByEmail');
		$this->assertFalse($this->user->loginByRememberMeToken());
		$this->assertCount(1, $this->user->getSetCookieCalls());
		$this->assertEquals([['Remember2']], $this->user->getSetCookieCalls());
		unset($_COOKIE['Remember2']);
	}

	public function testLoginByRememberMeTokenUnknownUser()
	{
		$token = '12345678901234567890123456789012345678901234567890123456';
		$_COOKIE['Remember2'] = $token;
		$this->dbMock->expects($this->once())->method('getRememberMeToken')->with($token)->willReturn(
			[
				'email'     => self::ENCRYPTED_EMAIL,
				'ua'        => self::ENCRYPTED_UA,
				'expire_ts' => time() + 10,
			]
		);
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn([]);
		$this->dbMock->expects($this->never())->method('logUserLogin');
		$this->dbMock->expects($this->once())->method('removeRememberMeTokenByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->assertFalse($this->user->loginByRememberMeToken());
		$this->assertCount(1, $this->user->getSetCookieCalls());
		$this->assertEquals([['Remember2']], $this->user->getSetCookieCalls());
		unset($_COOKIE['Remember2']);
	}

	public function testLoginByRememberMeTokenNotActiveUser()
	{
		$token = '12345678901234567890123456789012345678901234567890123456';
		$_COOKIE['Remember2'] = $token;
		$this->dbMock->expects($this->once())->method('getRememberMeToken')->with($token)->willReturn(
			[
				'email'     => self::ENCRYPTED_EMAIL,
				'ua'        => self::ENCRYPTED_UA,
				'expire_ts' => time() + 10,
			]
		);
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['active' => 0]);
		$this->dbMock->expects($this->never())->method('logUserLogin');
		$this->dbMock->expects($this->once())->method('removeRememberMeTokenByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->assertFalse($this->user->loginByRememberMeToken());
		$this->assertCount(1, $this->user->getSetCookieCalls());
		$this->assertEquals([['Remember2']], $this->user->getSetCookieCalls());
		unset($_COOKIE['Remember2']);
	}

	public function testLoginByRememberMeTokenExpiredToken()
	{
		$token = '12345678901234567890123456789012345678901234567890123456';
		$_COOKIE['Remember2'] = $token;
		$this->dbMock->expects($this->once())->method('getRememberMeToken')->with($token)->willReturn(['email' => self::ENCRYPTED_EMAIL, 'expire_ts' => time()]);
		$this->dbMock->expects($this->never())->method('getUserByEmail');
		$this->dbMock->expects($this->never())->method('logUserLogin');
		$this->dbMock->expects($this->once())->method('removeRememberMeTokenByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->assertFalse($this->user->loginByRememberMeToken());
		$this->assertCount(1, $this->user->getSetCookieCalls());
		$this->assertEquals([['Remember2']], $this->user->getSetCookieCalls());
		unset($_COOKIE['Remember2']);
	}

	public function testLoginByRememberMeTokenLoginSuccess()
	{
		$token = '12345678901234567890123456789012345678901234567890123456';
		$_COOKIE['Remember2'] = $token;
		$this->dbMock->expects($this->once())->method('getRememberMeToken')->with($token)->willReturn(
			[
				'id'        => 37,
				'email'     => self::ENCRYPTED_EMAIL,
				'ua'        => self::ENCRYPTED_UA,
				'expire_ts' => time() + 10,
				'password'  => 'a',
			]
		);
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['id' => '3', 'active' => 1, 'email' => self::ENCRYPTED_EMAIL]);
		$this->dbMock->expects($this->once())->method('logUserLogin')->with('3', self::ENCRYPTED_IP, self::ENCRYPTED_UA, 'rem');
		$this->dbMock->expects($this->never())->method('removeRememberMeTokenByEmail');
		$rememberToken = '';
		$this->dbMock->expects($this->once())->method('updateRememberMeToken')->with(
			37,
			$this->callback(
				function ($token) use (&$rememberToken) {
					$rememberToken = $token;

					return strlen($token) === 56;
				}
			),
			self::ENCRYPTED_IP,
			self::ENCRYPTED_UA
		);
		$this->assertTrue($this->user->loginByRememberMeToken());
		$this->assertCount(1, $this->user->getSetCookieCalls());
		$this->assertEquals([['Remember2', $rememberToken, time() + 86400 * 2, '/', '', true, true]], $this->user->getSetCookieCalls());
		$this->assertArrayHasKey('user', $_SESSION);
		$this->assertArrayNotHasKey('password', $_SESSION['user']);
		$this->assertArrayHasKey('email', $_SESSION['user']);
		$this->assertArrayHasKey('via', $_SESSION['user']);
		$this->assertArrayHasKey('ip', $_SESSION['user']);
		$this->assertArrayHasKey('ua', $_SESSION['user']);
		$this->assertSame('my@email.com', $_SESSION['user']['email']);
		$this->assertSame('rem', $_SESSION['user']['via']);
		$this->assertSame('138.161.213.58', $_SESSION['user']['ip']);
		$this->assertSame('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', $_SESSION['user']['ua']);
		unset($_SESSION['user']);
		unset($_COOKIE['Remember2']);
	}

	public function testLoginByRememberMeTokenLoginUserAgentNotMatch()
	{
		$token = '12345678901234567890123456789012345678901234567890123456';
		$_COOKIE['Remember2'] = $token;
		$this->dbMock->expects($this->once())->method('getRememberMeToken')->with($token)->willReturn(
			['id' => 37, 'email' => self::ENCRYPTED_EMAIL, 'ua' => 'mi vb', 'expire_ts' => time() + 10]
		);
		$this->dbMock->expects($this->never())->method('getUserByEmail');
		$this->dbMock->expects($this->never())->method('logUserLogin');
		$this->dbMock->expects($this->once())->method('removeRememberMeTokenByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->assertFalse($this->user->loginByRememberMeToken());
		$this->assertCount(1, $this->user->getSetCookieCalls());
		$this->assertEquals([['Remember2']], $this->user->getSetCookieCalls());
		unset($_COOKIE['Remember2']);
	}

	public function testLoginByRememberMeTokenLoginSuccessAgentSimilar()
	{
		$token = '12345678901234567890123456789012345678901234567890123456';
		$_COOKIE['Remember2'] = $token;
		$this->dbMock->expects($this->once())->method('getRememberMeToken')->with($token)->willReturn(
			[
				'id'        => 37,
				'email'     => self::ENCRYPTED_EMAIL,
				'ua'        => self::ENCRYPTED_UA_SIMILAR,
				'expire_ts' => time() + 10,
			]
		);
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['id' => '3', 'active' => 1, 'email' => self::ENCRYPTED_EMAIL]);
		$this->dbMock->expects($this->once())->method('logUserLogin')->with('3', self::ENCRYPTED_IP, self::ENCRYPTED_UA, 'rem');
		$this->dbMock->expects($this->never())->method('removeRememberMeTokenByEmail');
		$rememberToken = '';
		$this->dbMock->expects($this->once())->method('updateRememberMeToken')->with(
			37,
			$this->callback(
				function ($token) use (&$rememberToken) {
					$rememberToken = $token;

					return strlen($token) === 56;
				}
			),
			self::ENCRYPTED_IP,
			self::ENCRYPTED_UA
		);
		$this->assertTrue($this->user->loginByRememberMeToken());
		$this->assertCount(1, $this->user->getSetCookieCalls());
		$this->assertEquals([['Remember2', $rememberToken, time() + 86400 * 2, '/', '', true, true]], $this->user->getSetCookieCalls());
		$this->assertArrayHasKey('user', $_SESSION);
		$this->assertArrayHasKey('email', $_SESSION['user']);
		$this->assertArrayHasKey('via', $_SESSION['user']);
		$this->assertArrayHasKey('via', $_SESSION['user']);
		$this->assertSame('my@email.com', $_SESSION['user']['email']);
		$this->assertSame('rem', $_SESSION['user']['via']);
		unset($_SESSION['user']);
		unset($_COOKIE['Remember2']);
	}

	public function testLoginByRememberMeTokenLoginSuccessIgnoringSimilarity()
	{
		$token = '123456789012345678901234567890123456789012345678901234';
		$_COOKIE['Remember3'] = $token;
		$user = new CustomUserNoSimilarity($this->dbMock, $this->captchaMock, '2001:db8:3333:4444:5555:6666:7777:8888', 'my ua');
		$this->dbMock->expects($this->once())->method('getRememberMeToken')->with($token)->willReturn(
			['id' => 37, 'email' => self::ENCRYPTED_EMAIL, 'ua' => 'your', 'expire_ts' => time() + 10]
		);
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['id' => '3', 'active' => 1, 'email' => self::ENCRYPTED_EMAIL]);
		$this->dbMock->expects($this->once())->method('logUserLogin')->with(
			'3',
			'/8nAxep6C7VegtD57KOyrZnppqksomyetGt3Z30QXIFTgyz8PZZWtKTGtTUKWe30',
			'daKy0Yl7CbQQ2HNGG5G66A==',
			'rem'
		);
		$this->dbMock->expects($this->never())->method('removeRememberMeTokenByEmail');
		$this->dbMock->expects($this->once())->method('updateRememberMeToken')->with(
			37,
			$this->callback(
				function ($token) use (&$rememberToken) {
					$rememberToken = $token;

					return strlen($token) === 54;
				}
			),
			'/8nAxep6C7VegtD57KOyrZnppqksomyetGt3Z30QXIFTgyz8PZZWtKTGtTUKWe30',
			'daKy0Yl7CbQQ2HNGG5G66A=='
		);
		$this->assertTrue($user->loginByRememberMeToken());
		unset($_SESSION['user']);
		unset($_COOKIE['Remember3']);
	}

	public function testLoginByIpUserLogged()
	{
		$_SESSION['user'] = true;
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->loginByIp());
		unset($_SESSION['user']);
	}

	public function testLoginByIpNoUser()
	{
		$this->dbMock->expects($this->once())->method('getUserByIp')->with(self::ENCRYPTED_IP);
		$this->assertFalse($this->user->loginByIp());
	}

	public function testLoginByIpUserNotActive()
	{
		$this->dbMock->expects($this->once())->method('getUserByIp')->with(self::ENCRYPTED_IP)->willReturn(['active' => '0']);
		$this->assertFalse($this->user->loginByIp());
	}

	public function testLoginByIpSuccess()
	{
		$this->dbMock->expects($this->once())->method('getUserByIp')->with(self::ENCRYPTED_IP)->willReturn(
			['id' => '947', 'active' => '1', 'email' => self::ENCRYPTED_EMAIL, 'password' => 'a']
		);
		$this->dbMock->expects($this->once())->method('logUserLogin')->with('947', self::ENCRYPTED_IP, self::ENCRYPTED_UA, 'ip');
		$this->assertTrue($this->user->loginByIp());
		$this->assertArrayHasKey('user', $_SESSION);
		$this->assertArrayNotHasKey('password', $_SESSION['user']);
		$this->assertArrayHasKey('id', $_SESSION['user']);
		$this->assertArrayHasKey('email', $_SESSION['user']);
		$this->assertArrayHasKey('via', $_SESSION['user']);
		$this->assertArrayHasKey('ip', $_SESSION['user']);
		$this->assertArrayHasKey('ua', $_SESSION['user']);
		$this->assertSame('947', $_SESSION['user']['id']);
		$this->assertSame('my@email.com', $_SESSION['user']['email']);
		$this->assertSame('ip', $_SESSION['user']['via']);
		$this->assertSame('138.161.213.58', $_SESSION['user']['ip']);
		$this->assertSame('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', $_SESSION['user']['ua']);
		unset($_SESSION['user']);
	}

	public function testLogoutNotLoggedUser()
	{
		$this->dbMock->expects($this->never())->method('removeRememberMeTokenByEmail');
		$this->assertCount(0, $this->user->getSetCookieCalls());
		$this->assertFalse($this->user->logout());
	}

	public function testLogoutLoggedUser()
	{
		$_SESSION['user']['email'] = 'my@email.com';
		$this->dbMock->expects($this->once())->method('removeRememberMeTokenByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->assertTrue($this->user->logout());
		$this->assertCount(1, $this->user->getSetCookieCalls());
		$this->assertEquals([['Remember2']], $this->user->getSetCookieCalls());
		$this->assertArrayNotHasKey('user', $_SESSION);
	}

	public function testRegisterPasswordsDoNotMatch()
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->captchaMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->register('my@email.com', 'p1', 'p2', false));
		$this->assertSame('Passwords do not match', $this->user->getError());
	}

	public function testRegisterPasswordTooShort()
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->captchaMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->register('my@email.com', '123', '123', false));
		$this->assertSame('Password must have at least 4 characters', $this->user->getError());
	}

	public static function emailNotValidDataProvider(): array
	{
		return [
			['my@email.'],
			['@email.com'],
			['myemail.com'],
			['my@email@me.com'],
		];
	}

	/**
	 * @param string $email
	 *
	 * @dataProvider emailNotValidDataProvider
	 */
	public function testRegisterEmailNotValid(string $email)
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->captchaMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->register($email, '1234', '1234', false));
		$this->assertSame('Email address is not valid', $this->user->getError());
	}

	public function testRegisterWrongCaptcha()
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->captchaMock->expects($this->once())->method('isActive')->willReturn(true);
		$this->captchaMock->expects($this->once())->method('isValid')->willReturn(false);
		$this->captchaMock->expects($this->once())->method('getErrMsg')->willReturn('captcha error');
		$this->assertFalse($this->user->register('my@email.com', '1234', '1234', false));
		$this->assertSame('captcha error', $this->user->getError());
	}

	public function testRegisterCaptchaNotActiveNoLogin()
	{
		$this->captchaMock->expects($this->never())->method('isValid');
		$this->captchaMock->expects($this->never())->method('getErrMsg');
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->dbMock->expects($this->once())->method('insertNewUser')->with(
			self::ENCRYPTED_EMAIL,
			$this->callback(
				function ($hash) {
					return password_verify('1234', $hash);
				}
			),
			true
		);
		$this->dbMock->expects($this->never())->method('logUserLogin');
		$this->dbMock->expects($this->never())->method('logFailedUserRegister');
		$this->assertTrue($this->user->register('my@email.com', '1234', '1234', false));
		$this->assertSame('', $this->user->getError());
		$this->assertArrayNotHasKey('user', $_SESSION);
	}

	public function testRegisterCaptchaActiveNoLogin()
	{
		$this->captchaMock->expects($this->once())->method('isActive')->willReturn(true);
		$this->captchaMock->expects($this->once())->method('isValid')->willReturn(true);
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->dbMock->expects($this->once())->method('insertNewUser')->with(
			self::ENCRYPTED_EMAIL,
			$this->callback(
				function ($hash) {
					return password_verify('1234', $hash);
				}
			),
			true
		);
		$this->dbMock->expects($this->never())->method('logUserLogin');
		$this->dbMock->expects($this->never())->method('logFailedUserRegister');
		$this->assertTrue($this->user->register('my@email.com', '1234', '1234', false));
		$this->assertSame('', $this->user->getError());
		$this->assertArrayNotHasKey('user', $_SESSION);
	}

	public function testRegisterWithLogin()
	{
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->dbMock->expects($this->once())->method('insertNewUser')->with(
			self::ENCRYPTED_EMAIL,
			$this->callback(
				function ($hash) {
					return password_verify('1234', $hash);
				}
			),
			true
		)->willReturn('14');
		$this->dbMock->expects($this->once())->method('logUserLogin')->with('14', self::ENCRYPTED_IP, self::ENCRYPTED_UA, 'reg');
		$this->dbMock->expects($this->never())->method('logFailedUserRegister');
		$this->assertTrue($this->user->register('my@email.com', '1234', '1234', true));
		$this->assertSame('', $this->user->getError());
		$this->assertArrayHasKey('user', $_SESSION);
		$this->assertArrayNotHasKey('password', $_SESSION['user']);
		$this->assertArrayHasKey('id', $_SESSION['user']);
		$this->assertArrayHasKey('email', $_SESSION['user']);
		$this->assertArrayHasKey('via', $_SESSION['user']);
		$this->assertArrayHasKey('created_at', $_SESSION['user']);
		$this->assertArrayHasKey('ip', $_SESSION['user']);
		$this->assertArrayHasKey('ua', $_SESSION['user']);
		$this->assertSame('14', $_SESSION['user']['id']);
		$this->assertSame('my@email.com', $_SESSION['user']['email']);
		$this->assertSame('reg', $_SESSION['user']['via']);
		$this->assertSame('138.161.213.58', $_SESSION['user']['ip']);
		$this->assertSame('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', $_SESSION['user']['ua']);
		unset($_SESSION['user']);
	}

	public function testRegisterUserAlreadyExists()
	{
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['a']);
		$this->dbMock->expects($this->never())->method('insertNewUser');
		$this->dbMock->expects($this->never())->method('logUserLogin');
		$this->dbMock->expects($this->once())->method('logFailedUserRegister')->with(self::ENCRYPTED_EMAIL, self::ENCRYPTED_IP, self::ENCRYPTED_UA);
		$this->assertFalse($this->user->register('my@email.com', '1234', '1234', true));
		$this->assertSame('Email already exists', $this->user->getError());
		$this->assertArrayNotHasKey('user', $_SESSION);
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([2], $this->user->getSleepCalls());
	}

	/**
	 * @param string $email
	 *
	 * @dataProvider emailNotValidDataProvider
	 */
	public function testRecoverEmailNotValid(string $email)
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->captchaMock->expects($this->never())->method($this->anything());
		$this->assertSame('', $this->user->recoverPassword($email));
		$this->assertSame('Email address is not valid', $this->user->getError());
	}

	public function testRecoverWrongCaptcha()
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->captchaMock->expects($this->once())->method('isActive')->willReturn(true);
		$this->captchaMock->expects($this->once())->method('isValid')->willReturn(false);
		$this->captchaMock->expects($this->once())->method('getErrMsg')->willReturn('captcha error');
		$this->assertSame('', $this->user->recoverPassword('my@email.com'));
		$this->assertSame('captcha error', $this->user->getError());
	}

	public function testRecoverCaptchaUnknownUser()
	{
		$this->captchaMock->expects($this->once())->method('isActive')->willReturn(true);
		$this->captchaMock->expects($this->once())->method('isValid')->willReturn(true);
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->dbMock->expects($this->once())->method('logFailedUserRecover')->with(self::ENCRYPTED_EMAIL, self::ENCRYPTED_IP, self::ENCRYPTED_UA);
		$this->dbMock->expects($this->never())->method('getRecoverTokenByEmail');
		$this->assertSame('', $this->user->recoverPassword('my@email.com'));
		$this->assertSame('Email not exists', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([2], $this->user->getSleepCalls());
	}

	public function testRecoverNotActiveUser()
	{
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['active' => 0]);
		$this->dbMock->expects($this->once())->method('logFailedUserRecover')->with(self::ENCRYPTED_EMAIL, self::ENCRYPTED_IP, self::ENCRYPTED_UA);
		$this->dbMock->expects($this->never())->method('getRecoverTokenByEmail');
		$this->assertSame('', $this->user->recoverPassword('my@email.com'));
		$this->assertSame('User account is suspended', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([2], $this->user->getSleepCalls());
	}

	public function testRecoverTokenExistsNotExpired()
	{
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['active' => 1]);
		$this->dbMock->expects($this->never())->method('logFailedUserRecover');
		$this->dbMock->expects($this->once())->method('getRecoverTokenByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['expire_ts' => time() + 2]);
		$this->dbMock->expects($this->never())->method('updateRecoverToken');
		$this->dbMock->expects($this->never())->method('insertRecoverToken');
		$this->assertSame('', $this->user->recoverPassword('my@email.com'));
		$this->assertSame('Recovery email already sent', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([2], $this->user->getSleepCalls());
	}

	public function testRecoverTokenExistsExpired()
	{
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['active' => 1]);
		$this->dbMock->expects($this->never())->method('logFailedUserRecover');
		$this->dbMock->expects($this->once())->method('getRecoverTokenByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['expire_ts' => time(), 'id' => 43]);
		$this->dbMock->expects($this->once())->method('updateRecoverToken')->with(
			43,
			$this->callback(
				function ($token) {
					return strlen($token) === 22;
				}
			),
			self::ENCRYPTED_IP,
			self::ENCRYPTED_UA
		);
		$this->dbMock->expects($this->never())->method('insertRecoverToken');
		$this->assertNotSame('', $this->user->recoverPassword('my@email.com'));
		$this->assertSame('', $this->user->getError());
	}

	public function testRecoverTokenNotExists()
	{
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['active' => 1]);
		$this->dbMock->expects($this->never())->method('logFailedUserRecover');
		$this->dbMock->expects($this->once())->method('getRecoverTokenByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->dbMock->expects($this->once())->method('insertRecoverToken')->with(
			self::ENCRYPTED_EMAIL,
			$this->callback(
				function ($token) {
					return strlen($token) === 22;
				}
			),
			self::ENCRYPTED_IP,
			self::ENCRYPTED_UA
		);
		$this->dbMock->expects($this->never())->method('updateRecoverToken');
		$this->assertNotSame('', $this->user->recoverPassword('my@email.com'));
		$this->assertSame('', $this->user->getError());
	}

	public function testGetEmailForResetTokenWrongToken()
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->assertSame('', $this->user->getEmailForResetToken('1234567890'));
		$this->assertSame('Invalid password recovery token', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([2], $this->user->getSleepCalls());
	}

	public function testGetEmailForResetTokenNotExists()
	{
		$this->dbMock->expects($this->once())->method('getRecoverToken')->with('12345678901234567890ab');
		$this->dbMock->expects($this->exactly(1))->method($this->anything());
		$this->assertSame('', $this->user->getEmailForResetToken('12345678901234567890ab'));
		$this->assertSame('Invalid password recovery token', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([2], $this->user->getSleepCalls());
	}

	public function testGetEmailForResetTokenExpired()
	{
		$this->dbMock->expects($this->once())->method('getRecoverToken')->with('12345678901234567890ab')->willReturn(['expire_ts' => time()]);
		$this->dbMock->expects($this->exactly(1))->method($this->anything());
		$this->assertSame('', $this->user->getEmailForResetToken('12345678901234567890ab'));
		$this->assertSame('Password recovery token is expired', $this->user->getError());
	}

	public function testGetEmailForResetToken()
	{
		$this->dbMock->expects($this->once())->method('getRecoverToken')->with('12345678901234567890ab')->willReturn(['expire_ts' => time() + 2, 'email' => self::ENCRYPTED_EMAIL]);
		$this->dbMock->expects($this->exactly(1))->method($this->anything());
		$this->assertSame('my@email.com', $this->user->getEmailForResetToken('12345678901234567890ab'));
		$this->assertSame('', $this->user->getError());
	}

	public function testResetPasswordPasswordsDoNotMatch()
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->resetPassword('my@email.com', 'p1', 'p2'));
		$this->assertSame('Passwords do not match', $this->user->getError());
	}

	public function testResetPasswordPasswordTooShort()
	{
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->resetPassword('my@email.com', '123', '123'));
		$this->assertSame('Password must have at least 4 characters', $this->user->getError());
	}

	public function testResetPasswordUnknownUser()
	{
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn([]);
		$this->dbMock->expects($this->never())->method('updateUserPassword');
		$this->assertFalse($this->user->resetPassword('my@email.com', '1234', '1234'));
		$this->assertSame('Unknown user for this recovery token', $this->user->getError());
		$this->assertCount(1, $this->user->getSleepCalls());
		$this->assertEquals([2], $this->user->getSleepCalls());
	}

	public function testResetPasswordSuccess()
	{
		$this->dbMock->expects($this->once())->method('getUserByEmail')->with(self::ENCRYPTED_EMAIL)->willReturn(['id' => 8]);
		$this->dbMock->expects($this->once())->method('updateUserPassword')->with(
			8,
			$this->callback(
				function ($hash) {
					return password_verify('1234', $hash);
				}
			)
		);
		$this->dbMock->expects($this->once())->method('removeRecoverTokenByEmail')->with(self::ENCRYPTED_EMAIL);
		$this->assertTrue($this->user->resetPassword('my@email.com', '1234', '1234'));
		$this->assertSame('', $this->user->getError());
	}

	public function testChangePasswordMissingOldPassword()
	{
		$_SESSION['user'] = ['via' => 'rem'];
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->changePassword('p1', 'p2', ''));
		$this->assertSame('Wrong old password', $this->user->getError());
		unset($_SESSION['user']);
	}

	public function testChangePasswordPasswordsDoNotMatch()
	{
		$_SESSION['user'] = ['via' => 'log'];
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->changePassword('p1', 'p2', ''));
		$this->assertSame('Passwords do not match', $this->user->getError());
		unset($_SESSION['user']);
	}

	public function testChangePasswordPasswordTooShort()
	{
		$_SESSION['user'] = ['via' => 'log'];
		$this->dbMock->expects($this->never())->method($this->anything());
		$this->assertFalse($this->user->changePassword('123', '123', ''));
		$this->assertSame('Password must have at least 4 characters', $this->user->getError());
		unset($_SESSION['user']);
	}

	public function testChangePasswordWrongOldPassword()
	{
		$_SESSION['user'] = ['via' => 'rem', 'id' => '3'];
		$this->dbMock->expects($this->once())->method('getUserPassword')->with(3)->willReturn('12345');
		$this->dbMock->expects($this->never())->method('updateUserPassword');
		$this->assertFalse($this->user->changePassword('1234', '1234', '12345'));
		$this->assertSame('Wrong old password', $this->user->getError());
		unset($_SESSION['user']);
	}

	public function testChangePasswordSuccessWithOldPassword()
	{
		$_SESSION['user'] = ['via' => 'rem', 'id' => '3'];
		$this->dbMock->expects($this->once())->method('getUserPassword')->with(3)->willReturn(password_hash('12345', PASSWORD_BCRYPT));
		$this->dbMock->expects($this->once())->method('updateUserPassword')->with(
			3,
			$this->callback(
				function ($hash) {
					return password_verify('1234', $hash);
				}
			)
		);
		$this->assertTrue($this->user->changePassword('1234', '1234', '12345'));
		$this->assertSame('', $this->user->getError());
		unset($_SESSION['user']);
	}

	public function testChangePasswordSuccessNoOldPassword()
	{
		$_SESSION['user'] = ['via' => 'log', 'id' => '3'];
		$this->dbMock->expects($this->once())->method('getUserPassword')->with(3)->willReturn('12345');
		$this->dbMock->expects($this->once())->method('updateUserPassword')->with(
			3,
			$this->callback(
				function ($hash) {
					return password_verify('1234', $hash);
				}
			)
		);
		$this->assertTrue($this->user->changePassword('1234', '1234', '12345'));
		$this->assertSame('', $this->user->getError());
		unset($_SESSION['user']);
	}

	public function testGetUserIdNotLogged()
	{
		$this->assertSame('', $this->user->getUserId());
	}

	public function testGetUserId()
	{
		$_SESSION['user'] = ['id' => '4'];
		$this->assertSame('4', $this->user->getUserId());
		unset($_SESSION['user']);
	}

	public function testGetUserEmailNotLogged()
	{
		$this->assertSame('', $this->user->getUserEmail());
	}

	public function testGetUserEmail()
	{
		$_SESSION['user'] = ['email' => 'my@rmail.com'];
		$this->assertSame('my@rmail.com', $this->user->getUserEmail());
		unset($_SESSION['user']);
	}

	public function testGetUserRegistrationTimeNotLogged()
	{
		$this->assertSame('', $this->user->getRegistrationTime());
	}

	public function testGetUserRegistrationTime()
	{
		$_SESSION['user'] = ['created_at' => '2018-12'];
		$this->assertSame('2018-12', $this->user->getRegistrationTime());
		unset($_SESSION['user']);
	}

	public function testIsOldUserPassRequiredNotLogged()
	{
		$this->assertFalse($this->user->isOldUserPassRequired());
	}

	public function testIsOldUserPassRequired()
	{
		$_SESSION['user'] = ['via' => 'log'];
		$this->assertFalse($this->user->isOldUserPassRequired());
		$_SESSION['user'] = ['via' => 'rem'];
		$this->assertTrue($this->user->isOldUserPassRequired());
		unset($_SESSION['user']);
	}

	public function testIsLoggedViaLogin()
	{
		$this->assertFalse($this->user->isLoggedViaLogin());
		$_SESSION['user'] = ['via' => 'log'];
		$this->assertTrue($this->user->isLoggedViaLogin());
		$_SESSION['user'] = ['via' => 'rem'];
		$this->assertFalse($this->user->isLoggedViaLogin());
		$_SESSION['user'] = ['via' => 'ip'];
		$this->assertFalse($this->user->isLoggedViaLogin());
		unset($_SESSION['user']);
	}

	public function testIsLoggedViaIp()
	{
		$this->assertFalse($this->user->isLoggedViaIp());
		$_SESSION['user'] = ['via' => 'log'];
		$this->assertFalse($this->user->isLoggedViaIp());
		$_SESSION['user'] = ['via' => 'rem'];
		$this->assertFalse($this->user->isLoggedViaIp());
		$_SESSION['user'] = ['via' => 'ip'];
		$this->assertTrue($this->user->isLoggedViaIp());
		unset($_SESSION['user']);
	}
}

class CustomUser extends User
{
	const MAX_LOGIN_ATTEMPTS = 20;
	const FAILED_LOGIN_DELAY_MULTIPLIER = 11;

	const REMEMBER_LENGTH = 28;
	const REMEMBER_COOKIE_NAME = 'Remember2';
	const REMEMBER_EXPIRE = 2;
	const REMEMBER_UA_MIN_SIMILARITY = 50;

	const PASSWORD_MIN_LENGTH = 4;
	const RECOVER_LENGTH = 11;

	private array $setCookieCalls = [];
	private array $sleepCalls = [];

	public function sleep(int $seconds): void
	{
		$this->sleepCalls[] = $seconds;
	}

	public function setCookie(string $name, string $value = '', int $expire = 0, string $path = '', string $domain = '', bool $secure = false, bool $httpOnly = false): void
	{
		$this->setCookieCalls[] = func_get_args();
	}

	public function getSetCookieCalls(): array
	{
		return $this->setCookieCalls;
	}

	public function getSleepCalls(): array
	{
		return $this->sleepCalls;
	}
}

class CustomUserNoSimilarity extends CustomUser
{
	const REMEMBER_LENGTH = 27;
	const REMEMBER_COOKIE_NAME = 'Remember3';
	const REMEMBER_UA_MIN_SIMILARITY = 0;
}

class CustomUserExtraLoginCheckFalse extends CustomUser
{
	protected function extraLoginUserCheck(array $user): bool
	{
		return false;
	}

}