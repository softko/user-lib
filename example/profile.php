<?php
if (!$user->isLogged()) {
	header('Location: /');
	exit;
}
if (isset($_GET['delete_acc'])) {
	if ($user->deleteUserAccount()) {
		header('Location: /');
		exit;
	}
}

if (isset($_POST['pass']['pass'], $_POST['pass']['pass2'])) {
	if ($user->changePassword($_POST['pass']['pass'], $_POST['pass']['pass2'], isset($_POST['pass']['pass_old']) ? $_POST['pass']['pass_old'] : '')) {
		header('Location: /profile');
		exit;
	}
}
echo $twig->getContent('profile', ['error' => $user->getError()]);