<?php
if ($user->isLogged()) {
	header('Location: /');
	exit;
}
if (isset($_POST['reg'], $_POST['reg']['email'], $_POST['reg']['pass'], $_POST['reg']['pass2'])) {
	if ($user->register($_POST['login']['email'], $_POST['login']['pass'], $_POST['login']['pass2'], true)) {
		header('Location: /');
		exit;
	}
}
echo $twig->getContent('register', ['error' => $user->getError(),]);