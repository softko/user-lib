<?php
if ($user->isLogged()) {
	header('Location: /');
	exit;
}
if (isset($_POST['login'], $_POST['login']['email'], $_POST['login']['pass']) && $_POST['login']['email']) {
	if ($user->login($_POST['login']['email'], $_POST['login']['pass'], isset($_POST['login']['remember']))) {
		header('Location: /');
		exit;
	}
}
$failedLoginCount = $user->getFailedLoginCount();
$twig->printContent(
	'login',
	[
		'error'          => $user->getError(),
		'failed_log_cnt' => $failedLoginCount ? $failedLoginCount : $db->getFailedLoginCountByIp(REQUEST_IP),
		'captcha'        => false,
	]
);