<?php
require __DIR__ . '/../vendor/autoload.php';
error_reporting(-1);
session_start();
$config = [
	'production' => true,
	'captcha'    => [
		'url'    => 'https://www.google.com/recaptcha/api/siteverify',
		'site'   => '',
		'secret' => '',
	],
	'db'         => [
		'name' => '',
		'host' => '',
		'user' => '',
		'pass' => '',
	],
];

$isProduction = $config['production'];
if (!$isProduction) {
	ini_set('display_errors', 'on');
}
define('REQUEST_IP', $_SERVER['REMOTE_ADDR']);
define('USER_AGENT', $_SERVER['HTTP_USER_AGENT']);

$pdo = new \PDO(
	'mysql:host=' . $config['host'] . ';dbname=' . $config['db']['name'] . ';port=3306;charset=utf8',
	$config['user'], $config['pass'], [1002 => 'SET NAMES utf8']
);
$db = new \Softko\Db\PdoDb($pdo);
$user = new \Softko\User\User($db, new \Softko\Captcha\ReCaptchaV2($config['captcha']), REQUEST_IP, USER_AGENT);

if (isset($_GET['logout']) || !$user->checkLoggedUser()) {
	$user->logout();
	header('Location: /');
	exit;
}

$user->loginByRememberMeToken();