<?php
if ($user->isLogged()) {
	header('Location: /');
	exit;
}
if (isset($_POST['rec'], $_POST['rec']['email'])) {
	$user->recover($_POST['rec']['email']);
}
echo $twig->getContent('recover', ['error' => $user->getError()]);