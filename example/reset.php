<?php
if ($user->isLogged() || !isset($_GET['w'])) {
	header('Location: /');
	exit;
}
$email = $user->getEmailForResetToken($_GET['w']);
if ($email && isset($_POST['ch'], $_POST['ch']['pass'], $_POST['ch']['pass2'])) {
	if ($user->reset($email, $_POST['ch']['pass'], $_POST['ch']['pass2'])) {
		header('Location: /login');
		exit;
	}
}
echo $twig->getContent('reset', ['error' => $user->getError()]);