<?php

$emailToCreate = '';
require_once __DIR__ . '/../src/parameters.php';

use Softko\Captcha\ReCaptchaV2;
use Softko\Db\PdoDb;

$config = new \Config($params);
if (!$config->get('pass_setup')) {
	header('HTTP/1.0 404 Not Found');
	exit;
}
define('REQUEST_IP', $_SERVER['REMOTE_ADDR']);
define('ADMIN_URL', '/');
$db = new PdoDb($config->getDbConnection());
// project User class
$user = new User($db, new ReCaptchaV2(['active' => false]), REQUEST_IP, $_SERVER['HTTP_USER_AGENT']);

// User creation
if ($emailToCreate) {
	$user->register($email, 'simplePassword', 'simplePassword', false);
	$user->recoverPassword($email);
} else {
	if ($user->isLogged() || !isset($_GET['w'])) {
		header('Location: ' . ADMIN_URL);
		exit;
	}
	$email = $user->getEmailForResetToken($_GET['w']);
	if ($email && isset($_POST['ch'], $_POST['ch']['pass'], $_POST['ch']['pass2'])) {
		if ($user->resetPassword($email, $_POST['ch']['pass'], $_POST['ch']['pass2'])) {
			header('Location: ' . ADMIN_URL);
			exit;
		}
	}
	echo '<form method="post">
		<div>' . $user->getError() . '</div>
		<div>' . $email . '</div>
		<div>
			<input type="password" name="ch[pass]" required minlength="8" placeholder="Password"/>
		</div>
		<div>
			<input type="password" name="ch[pass2]" required minlength="8" placeholder="Confirm Password"/>
		</div>
		<div>
			<button type="submit">Setup password</button>
		</div>
	</form>';
}