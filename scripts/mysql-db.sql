SET NAMES utf8;
SET
    time_zone = '+00:00';
SET
    sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE
    DATABASE `app`
    COLLATE 'utf8_general_ci';
USE
    `app`;

SET
    FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user`
(
    `id`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `email`      VARCHAR(255)     NOT NULL,
    `password`   VARCHAR(255)     NOT NULL,
    `active`     TINYINT(1)       NOT NULL,
    `created_at` DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email` (`email`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `app_user_remember`;
CREATE TABLE `app_user_remember`
(
    `id`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `token`      VARCHAR(255)     NOT NULL,
    `email`      VARCHAR(255)     NOT NULL,
    `ua`         VARCHAR(255)     NOT NULL,
    `expire_ts`  INT(10) UNSIGNED NOT NULL,
    `ip`         VARCHAR(64)      NOT NULL,
    `created_at` DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `token` (`token`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `app_user_recover`;
CREATE TABLE `app_user_recover`
(
    `id`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `token`      VARCHAR(255)     NOT NULL,
    `email`      VARCHAR(255)     NOT NULL,
    `ua`         VARCHAR(255)     NOT NULL,
    `expire_ts`  INT(10) UNSIGNED NOT NULL,
    `ip`         VARCHAR(64)      NOT NULL,
    `created_at` DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `token` (`token`),
    UNIQUE KEY `email` (`email`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `app_user_ip_login`;
CREATE TABLE `app_user_ip_login`
(
    `id`      INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT(10) UNSIGNED NOT NULL,
    `ip`      CHAR(64)         NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ip` (`ip`),
    KEY `user_id` (`user_id`),
    CONSTRAINT `app_user_ip_login_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`)
        ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `app_user_login`;
CREATE TABLE `app_user_login`
(
    `id`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id`    INT(10) UNSIGNED NOT NULL,
    `ip`         VARCHAR(64)      NOT NULL,
    `ua`         VARCHAR(255)     NOT NULL,
    `via`        VARCHAR(20)      NOT NULL,
    `created_at` DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `user_id` (`user_id`),
    CONSTRAINT `app_user_login_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `app_user_login_failed`;
CREATE TABLE `app_user_login_failed`
(
    `id`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `email`      VARCHAR(255)     NOT NULL,
    `ip`         CHAR(64)         NOT NULL,
    `ua`         VARCHAR(255)     NOT NULL,
    `msg`        VARCHAR(255)     NOT NULL,
    `created_at` DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `app_user_rec_failed`;
CREATE TABLE `app_user_rec_failed`
(
    `id`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `email`      VARCHAR(255)     NOT NULL,
    `ip`         VARCHAR(64)      NOT NULL,
    `ua`         VARCHAR(255)     NOT NULL,
    `created_at` DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `app_user_login_failed_history`;
CREATE TABLE `app_user_login_failed_history`
(
    `id`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `email`      VARCHAR(255)     NOT NULL,
    `ip`         VARCHAR(64)      NOT NULL,
    `ua`         VARCHAR(255)     NOT NULL,
    `msg`        VARCHAR(255)     NOT NULL,
    `created_at` DATETIME         NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

SET
    FOREIGN_KEY_CHECKS = 1;